local Fixture = require "Fixture"
local DestroyableFixture = Fixture:extend()

-- Use mlib for line-polygon intersection
local mlib = require "lib.mlib"

function DestroyableFixture:new(x, y, r, shape, world, pool, shard_count)
  self.super.new(self, x, y, r, shape, world)

  self.shape_points = { self.shape:getPoints() }
  self.pool = pool

  self.shard_count = shard_count or 25
  self._shard_polygon = {}

  self.broken = false

  self.mx = 0
  self.my = 0
end

function DestroyableFixture:update(dt)
  self.mx, self.my = love.mouse.getPosition()
end

function DestroyableFixture:draw()
  --self:shatter(self.mx, self.my)
end

local function get_intersection_point(x1, y1, x2, y2, shape)
  -- Use mlib instead of Box2D's rayCast because rayCast is not made to
  -- raytrace inside the shape but rather outside of the shape.
  -- Therefore, it requires ugly tricks to make it working properly (but
  -- it is an interesting improvement to do).

  local intersections = mlib.polygon.getLineIntersection(x1, y1, x2, y2, shape)

  for i=1,#intersections do
    local ix, iy = unpack(intersections[i])

    -- Check if intersection point is well placed (not the opposite direction)
    if (x2 - x1) / (ix - x1) > 0 then
      return ix, iy
    end
  end
end

local function point_iter(points)
  return coroutine.wrap(function ()
    for i=1,#points,2 do
      coroutine.yield(points[i], points[i + 1])
    end

    coroutine.yield()
  end)
end

function DestroyableFixture:shatter(x, y, force)
  if self.fixture:testPoint(x, y) then
    -- Translate to local points to ease computation.
    x, y = self.body:getLocalPoint(x, y)

    local next_intersection_point = coroutine.wrap(function ()
      for i=1,self.shard_count do
        local angle = (i - 1) * ((2 * math.pi) / self.shard_count)
        coroutine.yield(get_intersection_point(x, y, x + math.cos(angle),
          y + math.sin(angle), self.shape_points))
      end

      coroutine.yield()
    end)
    local next_shape_point = point_iter(self.shape_points)
    local vx, vy

    local ix, iy = next_intersection_point()
    local px, py = next_shape_point()

    local angle_target = math.atan2(iy, ix)

    vx, vy = math.cos(angle_target) * force, math.sin(angle_target) * force

    -- First intersection points
    local i0x, i0y = ix, iy

    local polygon = self._shard_polygon
    polygon[1], polygon[2], polygon[3], polygon[4] = x, y, ix, iy
    local i = 4 -- == #polygon

    repeat
      local angle = math.atan2(py, px)

      angle = angle < 0 and (angle + math.pi * 2) or angle
      angle_target = angle_target < 0 and (angle_target + math.pi * 2) or angle_target

      print(string.format("px = %g, py = %g, ix = %g, iy = %g\n%.2g\t%.2g\n", px, py, ix, iy, angle, angle_target))

      if angle >= angle_target then
        ix, iy = next_intersection_point()
        if not ix then
          -- close with first points
          ix, iy = i0x, i0y
        end

        angle_target = math.atan2(iy, ix)

        polygon[i + 1], polygon[i + 2] = ix, iy
        i = i + 2

        -- cleanup next part
        for j=i+1,#polygon do
          polygon[j] = nil
        end

        i = 0

        for x, y in point_iter(polygon) do
          print(string.format(" - (%g;%g)", x, y))
        end

        --[[if #polygon == 8 then
          love.graphics.setColor(1, 0, 0, 1)
          love.graphics.setLineWidth(3)
        else
          love.graphics.setColor(1, 1, 1, 1)
          love.graphics.setLineWidth(1)
        end]]
        local shard_shape = love.physics.newPolygonShape(polygon)
        local nx, ny = self.body:getPosition()
        local fixture = Fixture(nx, ny, 0, shard_shape, self.world, "dynamic")
        fixture.body:applyForce(vx, vy)
        self.pool:queue(fixture)

        --love.graphics.polygon("line", self.body:getWorldPoints(unpack(polygon)))

        polygon[1], polygon[2], polygon[3], polygon[4] = x, y, ix, iy
        i = 4 -- == #polygon

        vx, vy = (ix - x) * force, (iy - y) * force

      else
        -- use a shape point
        polygon[i + 1], polygon[i + 2] = px, py
        px, py = next_shape_point()
        i = i + 2
      end
    until (ix == i0x and iy == i0y) or not px

    self.pool:remove(function (e) return e == self end)
    self.pool:flush()
    --[[
    local angle_target = math.atan2(iy, ix) + math.pi * 2
    local wx, wy = self.body:getWorldPoint(x, y)
    love.graphics.circle("line", wx, wy, 50)

    love.graphics.setColor(0, 1, 0, 1)
    love.graphics.setPointSize(5)
    love.graphics.points(wx + 50 * math.cos(angle_target), wy + 50 * math.sin(angle_target))
    love.graphics.setColor(0, 1, 1, 1)
    love.graphics.points(self.body:getWorldPoint(ix, iy))
    love.graphics.setColor(1, 1, 0, 1)
    love.graphics.line(wx, wy, self.body:getWorldPoint(ix, iy))
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.points(self.body:getWorldPoints(unpack(shape_points)))
    ]]
  end
end

return DestroyableFixture
