local nata = require "lib.nata"
local baton = require "lib.baton"

local DestroyableFixture = require "DestroyableFixture"

local pool = nata.new {
  require "systems.FixtureDrawing",
  nata.oop
}

local cx, cy = love.graphics.getWidth() /2, love.graphics.getHeight() / 2
local world

function love.load()
  world = love.physics.newWorld(0, 250)

  local box = DestroyableFixture(cx, cy, 0, love.physics.newRectangleShape(200, 200), world, pool)

  pool:queue(box)
  pool:flush()
end

function love.update(dt)
    love.window.setTitle("FPS: " .. love.timer.getFPS()
    .. " | Memory: " .. math.floor(collectgarbage 'count') .. 'kb')

  world:update(dt)
  pool:process("update", dt, pool)
end

function love.draw()
  --love.graphics.setBackgroundColor(.3, .5, 1, 1)
  love.graphics.setLineWidth(1)

  pool:process("draw")
end

function love.mousepressed(x, y, button, istouch)
  pool:process("shatter", x, y, 250)
end

function love.keypressed()
  local box = DestroyableFixture(cx, cy, 0, love.physics.newRectangleShape(200, 200), world, pool)

  pool:queue(box)
  pool:flush()
end

function love.gamepadpressed(device, key)
  pool:process("keypressed", key, scancode, isrepeat)
end
