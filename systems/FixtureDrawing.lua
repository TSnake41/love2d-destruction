return {
  filter = { "fixture" },
  process = {
    draw = function (self)
      for _,self in ipairs(self.entities) do
        love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
      end
    end
  }
}
