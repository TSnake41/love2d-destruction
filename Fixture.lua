local RenderedFixture = require "lib.classic":extend()

function RenderedFixture:new(x, y, r, shape, world, t)
  t = t or "static"
  self.body = love.physics.newBody(world, x, y, t)
  self.shape = shape

  self.world = world
  self.fixture = love.physics.newFixture(self.body, self.shape)
end

function RenderedFixture:__index(a)
  if a == "x" then
    return self.body:getX()
  elseif a == "y" then
    return self.body:getY()
  elseif a == "r" then
    return self.body:getAngle()
  else
    return getmetatable(self)[a] or rawget(self, a)
  end
end

function RenderedFixture:remove()
  self.fixture:destroy()
  self.body:destroy()
end

return RenderedFixture
